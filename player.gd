extends CharacterBody3D

var forward_max_speed: float = 5
var backward_max_speed: float = 5

var desired_direction: Vector2 = Vector2.ZERO

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta):
	pass
	
func _physics_process(delta):
	velocity = (transform.basis * Vector3(desired_direction.x, 0.0, desired_direction.y)) * forward_max_speed
	move_and_slide()
	
func _input(event):
	if event.is_action_pressed("move_forward"):
		desired_direction.x = 1
	if event.is_action_released("move_forward"):
		desired_direction.x = 0
	
	if event.is_action_pressed("move_backward"):
		desired_direction.x = -1
	if event.is_action_released("move_backward"):
		desired_direction.x = 0
		
	if event.is_action_pressed("move_left"):
		desired_direction.y = -1
	if event.is_action_released("move_left"):
		desired_direction.y = 0

	if event.is_action_pressed("move_right"):
		desired_direction.y = 1
	if event.is_action_released("move_right"):
		desired_direction.y = 0
		
	if event is InputEventMouseMotion:
		rotate_y(-event.relative.x * 0.005)
		$"SpringArm3D".rotate_z(-event.relative.y * 0.005)
		
